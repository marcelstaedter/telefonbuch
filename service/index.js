const express = require('express');
const path = require('path');
const { ApolloServer, gql } = require('apollo-server-express');
const glue = require('schemaglue');
const { schema, resolver } = glue('./service/gql');
const { ApolloServerPluginLandingPageGraphQLPlayground, ApolloServerPluginLandingPageDisabled } = require('apollo-server-core');

function isPlayGroundEnabled() {
    if (process.env.ENVIRONMENT == 'DEV' || process.env.ENVIRONMENT == null) return ApolloServerPluginLandingPageGraphQLPlayground({ footer: false });
    else return ApolloServerPluginLandingPageDisabled();
}

var typeDefs = gql(schema);

const server = new ApolloServer({
    typeDefs: typeDefs,
    resolvers: resolver,
    formatError: (error) => {
      //TODO: ERROR-Handling
      if (error && error.extensions && error.extensions.code === 'WRONG_CREDENTIALS') return console.error(error);
      if (error.originalError != null) {
        console.error(error.originalError.message, error, error.source);
        return { message: error.originalError.message, error: error.originalError };
      } else {
        console.error(error.message, error);
        if (error.extensions && error.extensions.response && error.extensions.response.body && error.extensions.response.body.errors) return { message: error.message, error: error.extensions.response.body.errors };
        return { message: error.message, error: error };
      }
    },
    context: async ({ req }) => {
      //TODO: AUTH
      //console.log('Authorization:' + global.token.includes(req.headers.authorization));
      if (process.env.DEV === 'TRUE') return;
      return req;
    },
    plugins: [ isPlayGroundEnabled() ],
});
  

server.start().then(() => {
    const app = express();
    server.applyMiddleware({ app });
    
    app.use(express.static(path.join(__dirname, '../app/build')));
    app.get('/phonebook', function(req,res) {
      res.sendFile(path.join(__dirname, '../app/build/index.html'));
    });
    
    app.listen({ port: 9090 }, (res) => {
    console.log(`🚀 Service ready at http://localhost:9090/graphql`);
    console.log(`Server ISO Time: ${new Date().toISOString()}`);
    console.log('Timezone-Offset: ' + new Date().getTimezoneOffset() / -60 + ' h');
    });
});
