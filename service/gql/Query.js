exports.resolver = {
  Query: {
    phoneBook: async function (obj, args) {
      return await getPhoneBook(args.name);
    },
  },
};

async function getPhoneBook(name = null) {
  const phonebook = require('./db/telefonbuch.json')
  return phonebook.filter(f => name == null || f.name.toLocaleLowerCase().includes(name.toLocaleLowerCase())).sort(function (a, b) {
    const nameA = a.name.toUpperCase(); // Groß-/Kleinschreibung ignorieren
    const nameB = b.name.toUpperCase(); // Groß-/Kleinschreibung ignorieren
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
  
    // Namen müssen gleich sein
    return 0;
  });
  // try {
  //   var phoneBookPath = _path.join(__dirname + './db/telefonbuch.json');
  //   var phoneBookBuffer = fs.readFileSync(phoneBookPath);
  //   var phoneBook = JSON.parse(phoneBookBuffer.toString());
  //   return phoneBook;
  // } catch (error) {
  //   console.error(error);
  //   return new ApolloError('Error in getPhoneBook', error.message, error );
  // }
}
