import { Component } from 'react'
import logo from './logo.svg';
import './App.css';
import RTable from './RTable';
import { TextField } from '@material-ui/core';
import {
  ApolloClient,
  InMemoryCache,
  gql
} from "@apollo/client";

const client = new ApolloClient({
  uri: 'http://127.0.0.1:9090/graphql',
  cache: new InMemoryCache()
});

class App extends Component {
  state = {
    // path: 'the-road-to-learn-react/the-road-to-learn-react',
    name: null,
    phoneBook: [],
    errors: null,
  };

  componentDidMount() {
    this.getPhoneBook();
  }  

  getPhoneBook(name = null) {
    client
    .query({
      variables: {name: name},
      query: gql`
        query($name: String) {
          phoneBook(name: $name)
          {
            name
            phone
          }
        }`
    })
    .then(r => { this.setState(() => ({ phoneBook: r.data.phoneBook, errors: r.data.errors, }))});
  }

  onChange = event => {
    this.setState({ name: event.target.value });
    this.getPhoneBook(event.target.value)
  };

  render() {
    const { errors, phoneBook } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1>Telefonbuch</h1>
        </header>
        <section>
          {errors ? <p>{errors}</p> : <p></p>}  
          <TextField label="Namen suchen" type="search" onChange={this.onChange} className="searchField"/>
          <RTable data={phoneBook}/>
        </section>
      </div>
    );
  }
}

export default App;
