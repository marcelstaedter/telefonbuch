import { Component } from 'react'
import { Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';

const RTableHeader = () => { 
    return (
        <TableHead>
            <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>Telefonnummer</TableCell>
            </TableRow>
        </TableHead>
    )
}

class RTableBody extends Component {    
    props = {
        entries: []
    };
    render() {
        const { entries } = this.props
        return (
            <TableBody>
                {entries.map(entry => (
                <TableRow>
                    <TableCell>{entry.name}</TableCell>
                    <TableCell>{entry.phone}</TableCell>
                </TableRow>
                ))}
            </TableBody>
        )
    }
}

class RTable extends Component {    
    props = {
    data: []
  };

  render() {
    const { data } = this.props;
    return (
      <Table>
        <RTableHeader />
        <RTableBody entries={data}/>
      </Table>
    )
  }
}

export default RTable;